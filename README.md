# PxE easy

Documentation and scripts to add PxE (Pre-boot eXecution Environment) to your local network.
It will allow you to install debian and ubuntu without any DVD or USB key, and much more. A must have at your local hackerspace !
You can also use it on a laptop for install parties.

PXE boot hides different services :
  * a DHCP serveur with BOOTP/PXE extensions. The service/server usually gives an IP address to your PC, now it will give the TFTP server IP and name of file to download and execute. I use "isc-dhcp-server" on debian/ubuntu.
  * a TFTP server to serve the needed files. We'll use pxelinux which comes from the syslinux project. It's easy to configure and uses the same configuration files for the menu as the isolinux system used by ubuntu and debian DVD.
  * a Web server for the preseed file which allows for automatic installation, or semi-automatic. 

# DHCP Configuration
If you already have a DHCP on your network, try to find PXE/BOOTP options. If it does not exist, two DHCP servers can co-exist but do not try that if you're not the network administrator. https://www.syslinux.org/wiki/index.php?title=PXELINUX
In short : *sudo apt-get install isc-dhcp-server*

In /etc/dhcp/dhcpd.conf add the lines for your network and lines about your TFTP server. Example :
```
ddns-update-style none;

allow booting;
allow bootp;

# option definitions common to all supported networks...
option domain-name "jack";
option domain-name-servers 8.8.8.8, 8.8.4.4;

default-lease-time 3600;
max-lease-time 7200;

# If this DHCP server is the official DHCP server for the local
# network, the authoritative directive should be uncommented.
authoritative;

subnet 192.168.42.0 netmask 255.255.255.0 {
  range 192.168.42.42 192.168.42.253;
  option domain-name-servers 192.168.42.1;
  option subnet-mask 255.255.255.0;
  option broadcast-address 192.168.42.255;
  option routers 192.168.42.1;            # Default gateway
  next-server 192.168.42.1;               # TFTP server, by it's IP address only
  filename "/pxelinux.0";
}

host gronica {  # Fixed IP address based on MAC address 
  hardware ethernet 00:20:6b:51:a8:f1;
  fixed-address 192.168.42.2;
}
```

# TFTP Configuration
List of working TFTP servers on the pxelinux wiki.
```sudo apt-get install tftpd-hpa```
You can add, if it does not exist, a */etc/default/tftpd-hpa* file. It will be used by the init script */etc/init.d/tftpd-hpa*. It's content :
```
# /etc/default/tftpd-hpa

TFTP_USERNAME="tftp"
TFTP_DIRECTORY="/var/lib/tftpboot"
TFTP_ADDRESS="0.0.0.0:69"
#TFTP_ADDRESS="[::]:69"
TFTP_OPTIONS="--secure"
```
You should now be able to start the server by typing and verifying it is running by checking the listening ports or the running process :
```
$ sudo /etc/init.d/tftp-hpa start
[ ok ] Starting tftpd-hpa (via systemctl): tftpd-hpa.service.
$ netstat -lnu |grep :69
udp        0      0 0.0.0.0:69              0.0.0.0:* 
$ ps aux |grep tftp
root      7323  0.0  0.0  15180   144 ?        Ss   15:41   0:00 /usr/sbin/in.tftpd --listen --user tftp --address 0.0.0.0:69 --secure /var/lib/tftpboot
```

# pxelinux menu
*mkmenu.sh* is a script to download some files and generate the debian and/or ubuntu pxelinux menu files to launch a netinst https://www.debian.org/distrib/netinst.
You can include them in the default menu (*/var/lib/tftpboot/pxelinux.cfg/default*), and add some entries.
A freedos entry, using memdisk kernel (copy it from */usr/lib/syslinux/memdisk* installed by the package *syslinux-common*) :
```label freedos
  menu label ^Freedos
        kernel memdisk
        append initrd=freedos.img raw
  text help
        Freedos
        endtext
```

# preseed
A preseed file is needed for automatic installation. It's prepared first with the help of *debconf-get-selections* command on a freshly installed system. Then you can just tweak the example file I provide for your environment.