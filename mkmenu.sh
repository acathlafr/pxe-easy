#!/bin/bash
# This script gets (if option "--no-DL" is not passed) the kernel files and initrd for the ubuntu and debian netinst and generates the pxelinux menu
# To add a new release, add it's name to the DISTS variable, separated by a space.
# If the name is not already in the list below, you can add it, just to show the version number in the menu
# https://wiki.ubuntu.com/Releases
# 
DST=/tftpboot	# TFTP server root
urlpreseed="http://jack/preseed"	# URL where the preseed files are (names are added later to that location)
distros="debian ubuntu"			# Which distributions you want (debian or ubuntu)
options="language=fr country=FR locale=fr_FR.UTF8 keymap=fr forcepae"	# options passed to the kernel at boot. Forcepae allows Pentium-M to be happy with modern i386 kernels

for d in $distros
do
	case $d in
		"debian")	DISTS="stretch";ARCHS="amd64";URL="http://ftp.free.fr/mirrors/ftp.debian.org/";;
		"ubuntu")	DISTS="xenial bionic";ARCHS="amd64 i386";URL="http://ftp.free.fr/mirrors/ftp.ubuntu.com/ubuntu/";;
	esac
	echo "Cleaning of $DST/${d}.menu"
	rm -f $DST/$d/${d}.menu
	for dist in $DISTS
	do
		for arch in $ARCHS
		do
		cat << EOF >> $DST/$d/${d}.menu
menu begin ${d}
        menu title ${d^}-${dist^}-${arch}
        label previous
        menu label Previous menu
        text help
        Return to previous menu
        endtext
        menu exit
        menu separator
        menu include ${d}/${dist}-${arch}.menu
menu end
EOF
			echo "Treating ${d^} ${dist^} ${arch} "
			case $dist in # Only to display the right version number 
				"stretch")      version="(9)";;
				"jessie")	version="(8)";;
				"wheezy")	version="(7)";;
				"squeeze")	version="(6)";;
				"lenny")	version="(5)";;
				"trusty")	version="(14.04)";;
				"vivid")	version="(15.04)";;
				"wily")		version="(15.10)";;
				"xenial")	version="(16.04)";;
				"zesty")	version="(17.04)";;
				"bionic")	version="(18.04)";;
				"cosmic")	version="(18.10)";;
			esac
			if [ "$1" != "--no-DL" ] 
			then
				echo "Getting files from internet for : ${d}"
				mkdir -p $DST/${d}/$dist-$arch
				wget -c -O $DST/${d}/$dist-$arch/linux "${URL}/dists/$dist/main/installer-$arch/current/images/netboot/${d}-installer/$arch/linux"
				wget -c -O $DST/${d}/$dist-$arch/initrd.gz "${URL}/dists/$dist/main/installer-$arch/current/images/netboot/${d}-installer/$arch/initrd.gz"
			fi
			rm -f $DST/${d}/${dist}-${arch}.menu
			cat << EOF >> $DST/${d}/${dist}-${arch}.menu
label $dist-$arch-netinst
  menu label automatic netinstall
  kernel ${d}/$dist-$arch/linux
  append initrd=${d}/$dist-$arch/initrd.gz url=$urlpreseed/${dist}auto.seed netcfg/dhcp_timeout=120 ${options} -- quiet
  text help
	Automatic installation of ${d^} $version
	Preconfigured proxy
  endtext

label $dist-$arch-netinst
  menu label ^netinstall ubuntu manual install
  kernel ${d}/$dist-$arch/linux
  append initrd=${d}/$dist-$arch/initrd.gz url=$urlpreseed/${dist}manual.seed netcfg/dhcp_timeout=120 ${options} -- quiet
  text help
	Manual installation of ${d^} $version
	Preconfigured proxy
  endtext

EOF
		done
	done
done

# Edit by hand your own default configuration file if it exists by including debian.menu and/or ubuntu.menu
if [ ! -f ${DST}/pxelinux.cfg/default ]
then
        cat << EOF >> ${DST}/pxelinux.cfg/default
default vesamenu.c32
include ubuntu.menu
include debian.menu

EOF
fi
